/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.midterm;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class TeastBathroom {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Bathroom map = new Bathroom(10, 10);
        Babypig babypig = new Babypig(0, 0, map);
        Shampoo shampoo = new Shampoo(5, 5);
        Foam foam = new Foam(3,2);
        map.setBabypig(babypig);
        map.setShampoo(shampoo);
        map.setFoam(foam);
        String str;
        while (true) {
            map.showMap();
            str = kb.next();
            if (str.equals("q")) {
                System.out.println("End program.");
            } else {
                babypig.walk(str);
            }
        }
    }
}
