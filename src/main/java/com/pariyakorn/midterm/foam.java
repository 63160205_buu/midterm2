/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.midterm;

/**
 *
 * @author acer
 */
public class Foam extends Obj{
    

    public Foam(int x, int y) {
        super(x,y);
        this.symbol = 'f';
    }

    @Override
    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }  
}
