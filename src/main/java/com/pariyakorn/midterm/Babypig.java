/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.midterm;

/**
 *
 * @author acer
 */
public class Babypig {
    private int x;
    private int y;
    private char symbol;
    private Bathroom map;
    
    Babypig(int x, int y, Bathroom map){
        this.x = x;
        this.y = y;
        this.symbol = 'p';
        this.map = map;
    
    }
    public boolean walk(char direction) {
        switch(direction){
            case 'N': 
            case 'w':    
                if(walkN())return false;
                break;
                
            case 'S':
            case 's':
                if(walkS())return false;
                break;
            case 'E':
            case 'd':
                if(walkE( ))return false;
                break;
            case 'W': 
            case 'a':    
                if(walkW( ))return false;
                break;
            default:
                return false;
        }
        checkShampoo();
        checkFoam();
        return true;
    }
    public boolean walk(String direction){
        if(direction.length() == 1){
            return walk((char)direction.charAt(0));
        }else{
            for(int i=0; i<direction.length(); i++){
                switch(direction.charAt(i)){
                    case 'N': 
                    case 'w':
                        if(walkN())return false;
                        break;

                    case 'S':
                    case 's':
                        if(walkS())return false;
                        break;
                    case 'E':
                    case 'd':
                        if(walkE( ))return false;
                        break;
                    case 'W': 
                    case 'a':
                        if(walkW( ))return false;
                        break;
                    default:
                        return false;
                }
                checkShampoo();
                checkFoam();
            }
            return true;
        }
    }
    
    private boolean walkW() {
        if (map.inMap(x - 1, y )) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkE() {
        if (map.inMap(x + 1, y )) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkS() {
        if (map.inMap(x , y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkN() {
        if (map.inMap(x , y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }
    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
    private void checkShampoo() {
        if(map.isShampoo(x,y)){
            System.out.println("pom pam(" + x + "," + y + ")" );
        }
    }
    private void checkFoam() {
        if(map.isFoam(x,y)){
            System.out.println("fresh fresh(" + x + "," + y + ")" );
        }
    }
}

